<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="org.json.JSONObject, java.net.URLDecoder" %>
<!Doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="./favicon.ico">
  <title>Review Article &middot; Hospital App</title>

  <!-- Bootstrap core CSS -->
  <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen,projection" />

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link type="text/css" rel="stylesheet" href="assets/css/style.css" />

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]> 
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<% 
    
    JSONObject publication = (JSONObject)request.getAttribute("publication");
    
    String title = URLDecoder.decode(publication.optString("title"), "UTF-8");
    String abstractContent = URLDecoder.decode(publication.optString("abstractContent"), "UTF-8");
    String introduction = URLDecoder.decode(publication.optString("introductionContent"), "UTF-8");
    String methods = URLDecoder.decode(publication.optString("methodsContent"), "UTF-8");
    String results = URLDecoder.decode(publication.optString("resultsContent"), "UTF-8");
    String discussions = URLDecoder.decode(publication.optString("discussionContent"), "UTF-8");
    String conclusion = URLDecoder.decode(publication.optString("conclusionContent"), "UTF-8");
    String references = URLDecoder.decode(publication.optString("referencesContent"), "UTF-8");

%>

<body>
  <!-- NAVBAR -->
  <div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
            aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <img src="assets/images/icon.png" class="logo" alt="Logo">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a href="#">Welcome
                <span id="user_name"></span>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)" onclick="logout();">Log out</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>

  <!-- MAIN -->
  <main>
    <div class="p-header">
      Review Article
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <a class="btn btn-default" href="/dashboard">
            <i class="glyphicon glyphicon-arrow-left"></i> Back</a>
        </div>
        <div class="col-md-6" style="text-align: right">

        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-5">
          <div class="col-md-11 col-md-offset-1">
            <div class="form-group row">
              <div class="col-sm-12">
                <p>
                  <label for="title">
                    <strong>Title:</strong>
                  </label>
                  <span id="title"><%= title %></span>
                </p>

              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="abstract">
                  <strong>Abstract</strong>
                </label>
                <p id="abstract">
                  <%= abstractContent %>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="intro">
                  <strong>Introduction</strong>
                </label>
                <p id="intro">
                  <%= introduction %>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="methods">
                  <strong>Methods</strong>
                </label>
                <p id="methods">
                  <%= methods %>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="results">
                  <strong>Results</strong>
                </label>
                <p id="results">
                  <%= results %>
                </p>
              </div>
            </div>
                <div class="form-group row">
              <div class="col-md-12">
                <label for="discussion">
                  <strong>Publication</strong>
                </label>
                <p id="discussion">
                  <%= discussions %>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="conclusion">
                  <strong>Conclusion</strong>
                </label>
                <p id="conslusion">
                  <%= conclusion %>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="references">
                  <strong>References</strong>
                </label>
                <p id="references">
                  <%= references %>
                </p>
              </div>
            </div>

          </div>

        </div>
        <div class="col-sm-12 col-md-7 col-lg-7">
          <div class="col-md-11 col-md-offset-1">
            <form accept-charset="UTF-8" role="form" id="articleForm" name "articleForm">
              <div class="form-group row">
                <div class="col-sm-12">
                  <h4 class="text-center">Score Sheet</h4>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <td></td>
                        <td></td>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="number">1</td>
                        <td class="title">Original idea/New Message</td>

                        <td class="score-radio">
                          <input type="radio" name="ideaMessage" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ideaMessage" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ideaMessage" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ideaMessage" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ideaMessage" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td class="number">2</td>
                        <td class="title">No Ethical Concerns(author COI, patient consent, etc)</td>

                        <td class="score-radio">
                          <input type="radio" name="ethicalConcerns" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ethicalConcerns" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ethicalConcerns" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ethicalConcerns" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="ethicalConcerns" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td class="number">3</td>
                        <td class="title">Good Abstract (Clear hypotesis, Justified conclusion)</td>

                        <td class="score-radio">
                          <input type="radio" name="goodAbstract" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="goodAbstract" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="goodAbstract" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="goodAbstract" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="goodAbstract" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td class="number">4</td>
                        <td class="title">Good and reproducible methodology</td>

                        <td class="score-radio">
                          <input type="radio" name="methodology" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="methodology" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="methodology" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="methodology" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="methodology" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td class="number">5</td>
                        <td class="title">Easy to read (Layout, Figures and tables, grammar, language)</td>

                        <td class="score-radio">
                          <input type="radio" name="easeToRead" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="easeToRead" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="easeToRead" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="easeToRead" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="easeToRead" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td class="number">6</td>
                        <td class="title">Strength and Limitation are clearly described</td>

                        <td class="score-radio">
                          <input type="radio" name="strengthLimitation" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="strengthLimitation" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="strengthLimitation" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="strengthLimitation" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="strengthLimitation" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td class="number">7</td>
                        <td class="title">Statistical review necessary</td>

                        <td class="score-radio">
                          <input type="radio" name="statReview" value="1" required>
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="statReview" value="2">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="statReview" value="3">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="statReview" value="4">
                        </td>
                        <td class="score-radio">
                          <input type="radio" name="statReview" value="5">
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="5">
                          <textarea class="form-control" placeholder="comment"></textarea>
                          <br />
                          <p>
                            Approve Article:
                            <span>
                              <input type="radio" id="approve" name="approveArticle" value="yes" required>
                              <label for="approve">Yes</label>

                              <input type="radio" id="unapprove" name="approveArticle" value="no">
                              <label for="unapprove">No</label>
                            </span>

                          </p>
                          <button class="btn btn-lg btn-info custom-bg " type="submit" id="reviewBtn">Submit</button>
                        </td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>

              </div>
            </form>
          </div>

        </div>

      </div>
  </main>

  <!-- FOOTER -->
  <footer id="footer">
    <div class="container">
      <div class="col-md-2">
        <p>
          <a href="#">Terms & Condition</a>
        </p>
      </div>
      <div class="col-md-2">
        <p>
          <a href="#">Privacy Policy</a>
        </p>
      </div>
      <div class="col-md-2">
        <p>
          <a href="#">About Us</a>
        </p>
      </div>
      <div class="col-md-2">
        <p>
          <a href="#">FAQs</a>
        </p>
      </div>
      <div class="col-md-2">
        <p>
          <a href="#">Feedback</a>
        </p>
      </div>
      <div class="col-md-2">
        <p>
          <a href="#">Contact Us</a>
        </p>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/js/util.js"></script>
  <script type="text/javascript" src="assets/js/review-articles.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>