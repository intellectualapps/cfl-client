(function() {
  $("#articleForm").on("submit", function(event) {
    event.preventDefault();
    $("#reviewBtn").addClass("disabled");
    var dataString = $("#articleForm").serialize();
    console.log(dataString);
    // submitReview(dataString);
  });
})();
function fetchArticle(dataString) {
  $.ajax({
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Bearer " + authToken);
    },
    method: "POST",
    url: "/api/v1/article",
    data: dataString,
    dataType: "json"
  })
    .done(function(response) {
      console.log(response);
      if (response.articleId) {
        console.log();
      }
    })
    .fail(function(error) {
      console.error(error);
    });
}

function populateFields(article) {
  $("#title").text(article.title.toUpperCase());
  $("#abstract").text(article.abstract);
  $("#intro").text(article.intro);
  $("#reference").text(article.intro);
}

function submitReview(dataString) {
  $.ajax({
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Bearer " + authToken);
    },
    method: "POST",
    url: "/api/v1/user/details",
    data: dataString,
    dataType: "json"
  })
    .done(function(response) {
      if (response.status) {
        $(".container-fluid").prepend(
          '<div class="alert alert-danger" id="reviewArticleMessage">' +
            response.message ||
            "An error occured, Please try again Later" + "</div>"
        );
        setTimeout(function() {
          $("#reviewArticleMessage").remove();
        }, 3000);
        $("#reviewBtn").removeClass("disabled");
      } else {
        $(".container-fluid").prepend(
          '<div class="alert alert-success" id="reviewArticleMessage">Your review has been submitted, Thank You.</div>'
        );
        setTimeout(function() {
          $("#reviewArticleMessage").remove();
          $("#reviewBtn").removeClass("disabled");
          window.location = "/dashboard";
        }, 3000);
      }
    })
    .fail(function(error) {
      $(".container-fluid").prepend(
        '<div class="alert alert-danger" id="reviewArticleMessage">' +
          error.message ||
          " An error occured, Please try again Later" + "</div>"
      );
      setTimeout(function() {
        $("#reviewArticleMessage").remove();
      }, 3000);
      $("#reviewBtn").removeClass("disabled");
    });
}
