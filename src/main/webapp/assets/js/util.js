const currentUrl = $(location).attr('href');
const authToken = getCookie('cfl-authToken');
const name = getCookie("cfl-name");
const email = getCookie("cfl-email");

/**
 * Plugins
 */
$(function () {
  $('[data-toggle="popover"]').popover();
  /**
   * Set user name
   */
  if (name) {
    $("#user_name").text(name);
  }
});

/**
 * Set Cookie 
 */
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Get Cookie 
 */
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

/**
 * Delete Cookie 
 */
function deleteCookie(name) {
  setCookie(name, "", -1);
}

/**
 * Log out
 */
function logout() {
  deleteCookie("cfl-name");
  deleteCookie("cfl-authToken");
  deleteCookie("cfl-email");
  window.location = '/login';
}


/**
 * Fetch Title
 */
function fetchTitle() {
  $.ajax({
      method: "GET",
      url: "/api/v1/titles",
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#titleId');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.id + '>' + value.description + '</option>');
        });
      }
    })
    .fail(function (error) {
      console.error(error);
    });
}

/**
 * Fetch Countries
 */
function fetchCountries() {
  $.ajax({
      method: "GET",
      url: "/api/v1/countries",
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#countryId');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.id + '>' + value.name + '</option>');
        });
      }
    })
    .fail(function (error) {
      console.error(error);
    });
}

/**
 * Fetch States
 */
function fetchStates(countryID) {
  $.ajax({
      method: "GET",
      url: "/api/v1/countries/states/" + countryID,
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#stateId');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.id + '>' + value.name + '</option>');
        });
      }
    })
    .fail(function (error) {
      console.error(error);
    });
}