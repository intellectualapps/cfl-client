$(function () {
  fetchTitle();
  fetchCountries();
  $("#fullname").text(name);
  $("#email").val(email);

  $("#addInterest").on("click", addInterest);
  $("#addPublication").on("click", addPublication);
  $("#changePassword").on("click", changePassword);
  $(".editPhoto").on("click", editPhoto);

  $("#UpdateDetailForm").on("submit", function (event) {
    event.preventDefault();
    $("#saveDetailsBtn").addClass("disabled");
    var email = $("#email").val();
    var firstName = $("#firstName").val();
    var lastName = $("#lastName").val();
    var phoneNumber = $("#phoneNumber").val();
    var location = $("#city").val();
    var photoUrl = $("#photoUrl").val();
    var title = $("#titleId").val();
    var state = $("#stateId").val();
    var country = $("#countryId").val();
    var city = $("#city").val();
    var institution = $("#institution").val();
    var dataString = "email=" + email + "&title=" + title + "&first-name=" + firstName + "&last-name=" + lastName + "&phoneNumber=" + phoneNumber + "&country=" + country + "&state=" + state + "&city=" + city + "&location=" + location + "&photoUrl=" + photoUrl + "&institution=" + institution;

    updateUserDetails(dataString);
  });

  fetchUserDetail(email);
});

function getState() {
  var countryId = $('#countryId').val();
  fetchStates(countryId);
}

function addLocation() {
  var city = $('#city').val();
  $('#location').val(city);
}

function addInterest() {
  window.location.href = "/interest";
}

function addPublication() {
  window.location.href = "/create-publication";
}

function changePassword() {
  alert("Change Password Link Clicked");
}

function editPhoto() {
  alert("Edit Photo Clicked");
}

function fetchUserDetail(email) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "GET",
      url: "/api/v1/user/details?email=" + email,
      dataType: "json"
    })
    .done(function (response) {
      if (response.status) {
        $(".container-fluid").prepend(
          '<div class="alert alert-danger" id="userDetailMessage">Failed to your detail, Please refresh your page </div>'
        );
      } else {
        populateForm(response);
      }
    })
    .fail(function (error) {
      $(".container-fluid").prepend(
        '<div class="alert alert-danger" id="userDetailMessage">Failed to your detail, Please refresh your page </div>'
      );
      setTimeout(function () {
        $("#userDetailMessage").remove();
      }, 3000);
    });
}

function updateUserDetails(userData) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "PUT",
      url: "/api/v1/user/details?" + userData,
      dataType: "json"
    })
    .done(function (response) {
      if (response.status) {
        $(".container-fluid").prepend('<div class="alert alert-danger" id="userDetailMessage">' + response.message || 'An error occured, Please try again Later' + "</div>");
        setTimeout(function () {
          $("#userDetailMessage").remove();
        }, 3000);
        $("#saveDetailsBtn").removeClass("disabled");
      } else {
        $(".container-fluid").prepend('<div class="alert alert-success" id="userDetailMessage">Personal details updated.</div>');
        setTimeout(function () {
          $('#userDetailMessage').remove();
        }, 3000);
        $("#saveDetailsBtn").removeClass("disabled");
      }
    })
    .fail(function (error) {
      $(".container-fluid").prepend('<div class="alert alert-danger" id="userDetailMessage">' + error.message || ' An error occured, Please try again Later' + "</div>");
      setTimeout(function () {
        $("#userDetailMessage").remove();
      }, 3000);
      $("#saveDetailsBtn").removeClass("disabled");
    });
}

function populateForm(userData) {
  $("#titleId").val(userData.title.id || "");
  $("#fullname").text(
    userData.firstName + " " + userData.lastName || name
  );
  $("#firstName").val(userData.firstName || "");
  $("#lastName").val(userData.lastName || "");
  $("#email").val(userData.email || email);
  $("#phoneNumber").val(userData.phoneNumber || "");
  $("#countryId").val(userData.country.id || "");
  getState();
  $("#stateId").val(userData.state.id || "");
  $("#city").val(userData.city || "");
  $("#institution").val(unescape(userData.institution) || "");
}