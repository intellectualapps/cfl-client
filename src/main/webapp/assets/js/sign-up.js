/**
 * Sign up
 */
(function () {
  $("#email").on("blur", function (event) {
    validateEmail();
  });

  $("#c_password").on("blur", function (event) {
    comparePassword();
  });

  var comppanyCounter = 1;
  $('#addCompanyBtn').on('click', function () {
    if (comppanyCounter >= 3) {
      $('#addCompanyBtn').addClass('disabled');
      return;
    }
    comppanyCounter++;
    addCompany(comppanyCounter);
  });

  $("input[name=coiOption]").on("click", function (event) {
    var optionValue = event.target.value;
    switch (optionValue) {
      case "company":
        $("#interest_type_div, #amount_id_div").attr("hidden", true);
        $("#company_div").attr("hidden", false);
        $("#company-name").focus();
        break;

      case "type":
        $("#company_div, #amount_id_div").attr("hidden", true);
        $("#interest_type_div").attr("hidden", false);
        break;

      case "amount":
        $("#company_div, #interest_type_div").attr("hidden", true);
        $("#amount_id_div").attr("hidden", false);
        break;

      default:
        $("#company_div, #interest_type_div, #amount_id_div").attr("hidden", true);
        break;
    }
  });

  $("#signUpForm").on("submit", function (event) {
    event.preventDefault();
    $("#signUpAlert").remove();
    $("#signUpBtn").addClass("disabled");
    $("#company_div").attr("hidden", false);
    $("#interest_type_div").attr("hidden", false);
    $("#amount_id_div").attr("hidden", false);
    var dataString = $('input[name!=c_password][name!=coiOption]', this).serialize();
    $.ajax({
        method: "POST",
        url: "http://165.227.75.157:8080/cflus/api/v1/user?" + dataString,
        dataType: "json"
      })
      .done(function (response) {
        console.log(response);
        if (response.authToken) {
          setCookie("cfl-email", response.email);
          setCookie("cfl-authToken", response.authToken);
          setCookie("cfl-name", response.firstName + " " + response.lastName);
          $("#signUpForm").prepend('<div class="alert alert-success" role="alert" id="signUpAlert">Registration successful!<hr>You will be logged-in in a moment</div>');
          setTimeout(function () {
            window.location = 'dashboard';
          }, 2000);
        } else {
          $("#company_div").attr("hidden", true);
          $("#interest_type_div").attr("hidden", true);
          $("#amount_id_div").attr("hidden", true);
          $("#signUpBtn").removeClass("disabled");
          $("#signUpForm").prepend('<div class="alert alert-danger" role="alert" id="signUpAlert">' + response.message + '</div>');
        }
      })
      .fail(function (error) {
        $("#company_div").attr("hidden", true);
        $("#interest_type_div").attr("hidden", true);
        $("#amount_id_div").attr("hidden", true);
        $("#signUpBtn").removeClass("disabled");
        $("#signUpForm").prepend('<div class="alert alert-danger" role="alert" id="signUpAlert">' + error.message + '</div>');
        $("#signUpBtn").removeClass("disabled");
      });
  });
})();

/**
 * Validate email
 */
function validateEmail() {
  $("#email_div").removeClass("has-error has-success");
  $("#messageBlock").remove();
  var email = $("#email").val();
  var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  if (!pattern.test(email)) {
    $("#email_div").addClass("has-error").removeClass("has-success");
    $("#email_div").append('<span id="messageBlock" class="help-block">Invalid Email address</span>');
    return false;
  } else {
    $("#email_div").addClass("has-success").removeClass("has-error");
    $("#messageBlock").remove();
    return true;
  }
}

/**
 * Compare password
 */
function comparePassword() {
  $("#c_password_div").removeClass("has-error has-success");
  $("#messageBlock").remove();
  var password = $("#password").val(),
    c_password = $("#c_password").val();
  if (c_password != password) {
    $("#c_password_div").addClass("has-error").removeClass("has-success");
    $("#c_password").attr("aria-describedby", "messageBlock");
    $("#c_password_div").append('<span id="messageBlock" class="help-block">Passwords do not match.</span>');
    return false;
  } else {
    $("#c_password_div").addClass("has-success").removeClass("has-error");
    return true;
  }
}

/**
 * Add company to COI
 */
function addCompany(companyCounter) {
  //
}