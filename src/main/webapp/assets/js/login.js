/**
 * Login
 */
(function () {
  $("#loginForm").on("submit", function (event) {
    event.preventDefault();
    $("#loginMessage").remove();
    $("#loginBtn").addClass("disabled");
    var dataString = $("#loginForm").serialize();
    $.ajax({
        method: "POST",
        url: "/cflus/api/v1/user/authenticate?"+dataString,
        dataType: "json"
      })
      .done(function (response) {
        if (response.authToken) {
          setCookie("cfl-email", response.email);
          setCookie("cfl-authToken", response.authToken);
          setCookie("cfl-name", response.firstName + " " + response.lastName);
          $("#loginForm").prepend('<div class="alert alert-success" id="loginMessage">Login successful</div>');
          setTimeout(function () {
            window.location = 'dashboard';
          }, 1000);
        } else {
          $("#loginBtn").removeClass("disabled");
          $("#loginForm").prepend('<div class="alert alert-danger" id="loginMessage">' + response.message + '</div>');
        }
      })
      .fail(function (error) {
        $("#loginBtn").removeClass("disabled");
        $("#loginForm").prepend('<div class="alert alert-danger" id="loginMessage">Login failed due to server error</div>');
      });
  });
})();