(function () {
  /**
   * Save article button
   */
  $("#saveArticleBtn").on("click", function () {
    $("#articleAlert").remove();
    $("#saveArticleBtn").addClass("disabled");

    // Get all form values
    var values = {};
    var fields = {};

    for (var instanceName in CKEDITOR.instances) {
      CKEDITOR.instances[instanceName].updateElement();
    }

    var dataString = $("#articleForm").serialize() + "&email=" + email;
    console.log(dataString);
    saveArticleForm(dataString);
  });

})();

/**
 * Save article
 */
function saveArticleForm(dataString) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "POST",
      url: "/create-publication",
      data: dataString,
      dataType: "json"
    })
    .done(function (response) {
      console.log(response);
      if (response.articleId) {          
          $("#articleForm").prepend('<div class="alert alert-success" role="alert" id="signUpAlert">Article saved!</div>');
          setTimeout(function () {
            window.location = '/dashboard';
          }, 2000);
        }
    })
    .fail(function (error) {
      console.error(error);
    });
}