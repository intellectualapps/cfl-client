/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client;

import com.cfl.client.http.PublicationServiceConsumer;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
/**
 *
 * @author ahassan
 */
public class ReviewArticle extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        boolean showDashboard = false;
        String authToken = "";

        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                String name = cookies[i].getName();
                if (name.equals("cfl-authToken")) {
                    authToken = cookies[i].getValue();
                    showDashboard = true;
                    break;
                }
            }
        }

        if (showDashboard) {
            String publicationId = request.getParameter("pid");
        
            String rawPublication = new PublicationServiceConsumer().getArticleDetails(publicationId, authToken);
            
            System.out.println("PUBLICATION: " + rawPublication);
            
            JSONObject publication = new JSONObject(rawPublication);
            
            request.setAttribute("publication", publication);
            request.getRequestDispatcher("WEB-INF/pages/review-article.jsp").forward(request, response);
        } else {
            response.sendRedirect("/login");
        }
    }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }

  @Override
  public String getServletInfo() {
    return "Servlet to manage the review article page 'review-article.html'";
  }

}
