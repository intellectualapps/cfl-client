/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client.bff;

import com.cfl.client.http.UserServiceConsumer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cfl.client.http.ServiceConsumer;

/**
 *8
 * @author buls
 */
public class UserResource extends HttpServlet {

    Gson gsonCreator;
    
    
    @Override
    public void init() throws ServletException {
        gsonCreator = new GsonBuilder().setPrettyPrinting().create();
    }    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        String email = request.getParameter("email");
        String rawToken = request.getHeader("Authorization");
        String serviceResponse = new UserServiceConsumer().getUserDetails(email, rawToken);
              
                     
       response.getWriter().write(serviceResponse);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String firstName = request.getParameter("first-name");
        String lastName = request.getParameter("last-name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
                
        System.out.println(firstName + " " + lastName + " " + email + " " + password);
        String serviceResponse = new ServiceConsumer().createUser(firstName, 
                lastName, email, password);
       
        response.getWriter().write(serviceResponse);
    }
    
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().write(gsonCreator.toJson("Hello from PUT"));
    }        

    @Override
    public String getServletInfo() {
        return "cfl User Resource BFF";
    }        

}
