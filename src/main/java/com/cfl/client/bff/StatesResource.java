package com.cfl.client.bff;

import com.cfl.client.http.CountryServiceConsumer;
import com.cfl.client.http.util.ServiceParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by makarfi on 1/7/18.
 */
public class StatesResource extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String requestPath = request.getRequestURI();
        String countryId = requestPath.substring(requestPath.lastIndexOf("/"), requestPath.length()).replace("/", "");

        String serviceResponse = new CountryServiceConsumer().getStatesByCountry(countryId);

        response.getWriter().write(serviceResponse);
    }

    @Override
    public String getServletInfo() {
        return "cfl States Resource BFF";
    }

}
