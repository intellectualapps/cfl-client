/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client.bff;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cfl.client.http.ServiceConsumer;

/**
 *8
 * @author buls
 */
public class UsernameResource extends HttpServlet {

    Gson gsonCreator;
    
    
    @Override
    public void init() throws ServletException {
        gsonCreator = new GsonBuilder().setPrettyPrinting().create();
    }    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
       
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String tempKey = request.getParameter("temp-key");
                
        String serviceResponse = new ServiceConsumer()
                .createUsername(username, email, tempKey);
       
        response.getWriter().write(serviceResponse);
    }        

    @Override
    public String getServletInfo() {
        return "Pentor User Resource BFF";
    }        

}
