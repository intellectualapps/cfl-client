/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client.bff;

import com.cfl.client.http.PublicationServiceConsumer;
import com.cfl.client.http.util.ServiceParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author itssadon
 */
public class PublicationsResource extends HttpServlet {

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String title = request.getParameter(ServiceParams.TITLE);
    String keywords = request.getParameter(ServiceParams.KEYWORDS);
    String commentsAllowed = request.getParameter(ServiceParams.COMMENTS_ALLOWED);
    String peerReviewType = request.getParameter(ServiceParams.PEER_REVIEW_TYPE);
    String abstractContent = request.getParameter(ServiceParams.ABSTRACT_CONTENT);
    String introductionContent = request.getParameter(ServiceParams.INTRODUCTION_CONTENT);
    String methodsContent = request.getParameter(ServiceParams.METHODS_CONTENT);
    String resultsContent = request.getParameter(ServiceParams.RESULTS_CONTENT);
    String discussionContent = request.getParameter(ServiceParams.DISCUSSION_CONTENT);
    String conclusionContent = request.getParameter(ServiceParams.CONCLUSION_CONTENT);
    String referencesContent = request.getParameter(ServiceParams.REFERENCES_CONTENT);
    String summary = request.getParameter(ServiceParams.SUMMARY);
    String email = request.getParameter(ServiceParams.EMAIL);
    String coAuthors = request.getParameter(ServiceParams.CO_AUTHORS);
    String rawToken = request.getHeader(ServiceParams.AUTHORIZATION);

    String serviceResponse = new PublicationServiceConsumer()
            .saveArticle(title, keywords, commentsAllowed, peerReviewType, abstractContent,
                    introductionContent, methodsContent, resultsContent, discussionContent,
                    conclusionContent, referencesContent, summary, email, coAuthors, rawToken);

    response.getWriter().write(serviceResponse);
  }

  @Override
  public String getServletInfo() {
    return "Publications servlet";
  }

}