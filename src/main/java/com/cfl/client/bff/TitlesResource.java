package com.cfl.client.bff;

import com.cfl.client.http.TitleServiceConsumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by makarfi on 1/7/18.
 */
public class TitlesResource extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String serviceResponse = new TitleServiceConsumer().getAllTitles();

        response.getWriter().write(serviceResponse);
    }

    @Override
    public String getServletInfo() {
        return "cfl Titles Resource BFF";
    }

}
