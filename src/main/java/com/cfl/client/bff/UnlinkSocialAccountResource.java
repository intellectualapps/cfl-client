/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client.bff;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cfl.client.http.ServiceConsumer;

/**
 *8
 * @author Lateefah
 */
public class UnlinkSocialAccountResource extends HttpServlet {

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestPath = request.getRequestURI();
        String username = requestPath
                .substring(requestPath.lastIndexOf("/"), requestPath.length())
                .replace("/", "");
        String platform = request.getParameter("platform");
        String rawToken = request.getHeader("Authorization");
                
        String serviceResponse = new ServiceConsumer().unlinkSocialAccount(username, platform, rawToken);
       
        response.getWriter().write(serviceResponse);
    }        
       
}
