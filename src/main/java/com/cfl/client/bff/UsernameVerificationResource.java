/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client.bff;

import com.cfl.client.http.ServiceConsumer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author buls
 */
public class UsernameVerificationResource extends HttpServlet {    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        String requestPath = request.getRequestURI();
        String username = requestPath
                .substring(requestPath.lastIndexOf("/"), requestPath.length())
                .replace("/", "");
        
        String serviceResponse = new ServiceConsumer().verifyUsername(username);                     
        
        response.getWriter().write(serviceResponse);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
