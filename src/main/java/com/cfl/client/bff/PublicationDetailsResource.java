package com.cfl.client.bff;

import com.cfl.client.http.PublicationServiceConsumer;
import com.cfl.client.http.util.ServiceParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by makarfi on 2/20/18.
 */
public class PublicationDetailsResource extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String publicationId = request.getParameter(ServiceParams.PUBLICATION_ID);
        String rawToken = request.getHeader(ServiceParams.AUTHORIZATION);

        String serviceResponse = new PublicationServiceConsumer().getArticleDetails(publicationId, rawToken);

        response.getWriter().write(serviceResponse);
    }

    @Override
    public String getServletInfo() {
        return "Publication details servlet";
    }
}
