package com.cfl.client.bff;

import com.cfl.client.http.UserServiceConsumer;
import com.cfl.client.http.util.ServiceParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by makarfi on 12/22/17.
 */
public class UserDetailsResource extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("email");
        String rawToken = request.getHeader("Authorization");

        String serviceResponse = new UserServiceConsumer().getUserDetails(email, rawToken);

        response.getWriter().write(serviceResponse);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter(ServiceParams.EMAIL);
        String firstName = request.getParameter(ServiceParams.FIRST_NAME);
        String lastName = request.getParameter(ServiceParams.LAST_NAME);
        String phoneNumber = request.getParameter(ServiceParams.PHONE_NUMBER);
        String location = request.getParameter(ServiceParams.LOCATION);
        String photoUrl = request.getParameter(ServiceParams.PHOTO_URL);
        String titleId = request.getParameter(ServiceParams.TITLE);
        String stateId = request.getParameter(ServiceParams.STATE);
        String countryId = request.getParameter(ServiceParams.COUNTRY);
        String city = request.getParameter(ServiceParams.CITY);
        String institution = request.getParameter(ServiceParams.INSTITUTION);

        String rawToken = request.getHeader(ServiceParams.AUTHORIZATION);

        String serviceResponse = new UserServiceConsumer().updateUserDetails(email, firstName, lastName, phoneNumber,
                location, photoUrl, titleId, stateId, countryId, city, institution, rawToken);

        response.getWriter().write(serviceResponse);
    }

    @Override
    public String getServletInfo() {
        return "cfl UserDetails Resource BFF";
    }

}
