/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client.bff;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cfl.client.http.ServiceConsumer;

/**
 *
 * @author buls
 */
public class UserAuthentication extends HttpServlet {

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String password = request.getParameter("password");
        String authType = request.getParameter("auth-type");
        
        String serviceResponse = new ServiceConsumer().authenticateUser(id, password, authType);
                     
        response.getWriter().write(serviceResponse);        
    }
   
    @Override
    public String getServletInfo() {
        return "User authentication servlet";
    }
}
