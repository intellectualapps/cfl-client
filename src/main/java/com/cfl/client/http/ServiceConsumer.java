/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Use request.setPayload(getPostData(map).getBytes(StandardCharsets.UTF_8));
 * in order to set request payload when needed
 */
package com.cfl.client.http;

import com.cfl.client.http.util.Util;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

import com.cfl.client.http.util.ServicePaths;
import com.cfl.client.http.util.ServiceParams;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;

/**
 *
 * @author buls
 */
public class ServiceConsumer {

    private static final HTTPHeader CONTENT_TYPE_HEADER = 
      new HTTPHeader("Content-Type", "application/x-www-form-urlencoded");     
    private final String PARAMETER_START = "?";
    private final String SLASH = "/";

    private URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();


    public String createUser(String firstName, String lastName, String email, String password) 
            throws IOException {

        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.FIRST_NAME, Util.encode(firstName));
        map.put(ServiceParams.LAST_NAME, Util.encode(lastName));
        map.put(ServiceParams.EMAIL, Util.encode(email));
        map.put(ServiceParams.PASSWORD, Util.encode(password));

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);

        //save user's promo code if provided
        return new String(httpResponse.getContent(), "UTF-8");
    }  
    
   /* public String createBillingAgreementToken(String accessToken) throws IOException {              

        String url = CREATE_BILLING_AGREEMENT_TOKEN_LIVE_ENDPOINT;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        //con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON_HEADER_VALUE);
        con.setRequestProperty(AUTHORIZATION_HEADER, accessToken);
        con.setDoOutput(true);
        
        OutputStream wr = con.getOutputStream();
        String data = buildDataForCreateBillingAgreementToken();
        System.out.println("DATA String: " + data);
        wr.write(data.getBytes("UTF-8"));
        
        int responseCode = con.getResponseCode();
        System.out.println("Sending 'POST' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }*/
    
    public String createUsername(String username, String email, String tempKey) 
            throws IOException {
        
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.USERNAME, Util.encode(username));
        map.put(ServiceParams.EMAIL, Util.encode(email));
        map.put(ServiceParams.TEMP_KEY, Util.encode(tempKey));

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.USERNAME_RESOURCE + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);

        //save user's promo code if provided
        return new String(httpResponse.getContent(), "UTF-8");
    }  
    
    public String verifyUsername(String username) throws IOException {
        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.USERNAME_VERIFICATION_RESOURCE +
                SLASH + username;
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);            

        try {
            HTTPResponse httpResponse = urlFetchService.fetch(request);
            return new String(httpResponse.getContent(), "UTF-8");
        } catch (SocketTimeoutException ste) {
            return "{}";
        }
        
    }
    
    public String authenticateUser(String id, String password, String authType) 
            throws IOException {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.ID, Util.encode(id));
        map.put(ServiceParams.PASSWORD, Util.encode(password));
        map.put(ServiceParams.AUTH_TYPE, Util.encode(authType));

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.USER_AUTHENTICATION_RESOURCE
                + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);

        return new String(httpResponse.getContent(), "UTF-8");
    }
    
    public String initPasswordReset(String email) throws IOException {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.EMAIL, Util.encode(email));

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.USER_PASSWORD_RESOURCE
                + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);

        return "{}"; //new String(httpResponse.getContent(), "UTF-8");
    }
    

    
    public String verifyEmail(String email) throws IOException {
        
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.EMAIL, Util.encode(email));
        
        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.VERIFY_EMAIL_PATH + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);

        return new String(httpResponse.getContent(), "UTF-8");
    }
    
    public String verifyUserSmsCode(String email, String smsCode) throws IOException {
        
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.EMAIL, Util.encode(email));
        map.put(ServiceParams.LAST_NAME, Util.encode(smsCode));

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + SLASH + email + ServicePaths.SMS_CODE_RESOURCE + SLASH + smsCode;
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        
        return new String(httpResponse.getContent(), "UTF-8");
    }
    
    public String resendSmsVerificationCode(String email) throws IOException {   
        if (email == null || email.isEmpty()) {
            email = "no@user.com";
        }
        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + SLASH + email + ServicePaths.SMS_CODE_RESOURCE;
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        
        return new String(httpResponse.getContent(), "UTF-8");
    }        
    
    public String resendEmailVerificationLink(String email) throws IOException {   
        if (email == null || email.isEmpty()) {
            email = "no@user.com";
        }
        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + SLASH + email + ServicePaths.EMAIL_LINK_RESOURCE;
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        
        return new String(httpResponse.getContent(), "UTF-8");
    }

    public String authenticateUserPasswordReset(String encodedEmail, String token) throws IOException{
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.EMAIL, Util.encode(encodedEmail));
        map.put(ServiceParams.PASSWORD_RESET_TOKEN, Util.encode(token));

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.USER_PASSWORD_TOKEN_RESOURCE
                + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);        

        HTTPResponse httpResponse = urlFetchService.fetch(request);

        return new String(httpResponse.getContent(), "UTF-8");
    }
     
    public String saveDeviceToken(String email, String deviceToken, 
            String uuid, String platform, String rawToken) throws IOException {                

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.DEVICE_TOKEN_RESOURCE
                + SLASH + email + SLASH + deviceToken + SLASH + uuid + SLASH + platform;
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));
        
        HTTPResponse httpResponse = urlFetchService.fetch(request);        
        return new String(httpResponse.getContent(), "UTF-8");
    }    
    

    
    public String linkSocialAccount (String username, String platform, String email,
            String rawToken) throws IOException {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.PLATFORM, Util.encode(platform));
        map.put(ServiceParams.EMAIL, Util.encode(email));
        
        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.LINK_SOCIAL_RESOURCE + SLASH + username + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.PUT);
        request.addHeader(CONTENT_TYPE_HEADER);        
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }
    
    public String unlinkSocialAccount (String username, String platform, String rawToken) throws IOException {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ServiceParams.PLATFORM, Util.encode(platform));
        
        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + ServicePaths.UNLINK_SOCIAL_RESOURCE + SLASH + username + PARAMETER_START + Util.getData(map);
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.PUT);
        request.addHeader(CONTENT_TYPE_HEADER);        
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }

}
