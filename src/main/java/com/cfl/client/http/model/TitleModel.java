package com.cfl.client.http.model;

/**
 * Created by makarfi on 12/20/17.
 */
public class TitleModel {

    private String id;
    private String description;

    public TitleModel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
