package com.cfl.client.http.model;

/**
 * Created by makarfi on 12/20/17.
 */
public class StateModel {

    private String id;
    private String name;

    public StateModel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
