package com.cfl.client.http.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makarfi on 2/20/18.
 */
public class ArticleModel {

    private String title;
    private String name;
    private String keywords;
    private String commentsAllowed;
    private String abstractContent;
    private String introductionContent;
    private String methodsContent;
    private String resultsContent;
    private String discussionContent;
    private String conclusionContent;
    private String referencesContent;

    private UserModel author;
    private String summary;
    private String peerReviewType;
    private List<UserModel> coAuthors = new ArrayList<>();    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCommentsAllowed() {
        return commentsAllowed;
    }

    public void setCommentsAllowed(String commentsAllowed) {
        this.commentsAllowed = commentsAllowed;
    }

    public String getAbstractContent() {
        return abstractContent;
    }

    public void setAbstractContent(String abstractContent) {
        this.abstractContent = abstractContent;
    }

    public String getIntroductionContent() {
        return introductionContent;
    }

    public void setIntroductionContent(String introductionContent) {
        this.introductionContent = introductionContent;
    }

    public String getMethodsContent() {
        return methodsContent;
    }

    public void setMethodsContent(String methodsContent) {
        this.methodsContent = methodsContent;
    }

    public String getResultsContent() {
        return resultsContent;
    }

    public void setResultsContent(String resultsContent) {
        this.resultsContent = resultsContent;
    }

    public String getDiscussionContent() {
        return discussionContent;
    }

    public void setDiscussionContent(String discussionContent) {
        this.discussionContent = discussionContent;
    }

    public String getConclusionContent() {
        return conclusionContent;
    }

    public void setConclusionContent(String conclusionContent) {
        this.conclusionContent = conclusionContent;
    }

    public String getReferencesContent() {
        return referencesContent;
    }

    public void setReferencesContent(String referencesContent) {
        this.referencesContent = referencesContent;
    }

    public UserModel getAuthor() {
        return author;
    }

    public void setAuthor(UserModel author) {
        this.author = author;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getPeerReviewType() {
        return peerReviewType;
    }

    public void setPeerReviewType(String peerReviewType) {
        this.peerReviewType = peerReviewType;
    }

    public List<UserModel> getCoAuthors() {
        return coAuthors;
    }

    public void setCoAuthors(List<UserModel> coAuthors) {
        this.coAuthors = coAuthors;
    }
}
