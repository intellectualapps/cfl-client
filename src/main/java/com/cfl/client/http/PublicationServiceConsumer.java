package com.cfl.client.http;

import com.cfl.client.http.model.ArticleModel;
import com.cfl.client.http.model.UserModel;
import com.cfl.client.http.util.ServiceParams;
import com.cfl.client.http.util.ServicePaths;
import com.cfl.client.http.util.Util;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

/**
 * Created by makarfi on 2/20/18.
 */
public class PublicationServiceConsumer {

    private static final HTTPHeader CONTENT_TYPE_HEADER =
            new HTTPHeader("Content-Type", "application/json; charset=UTF-8");

    private static final String PARAMETER_START = "?";
    private static final String SLASH = "/";
    private static final String HASH_PATH = "hash";

    private static URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();
    private static Gson _gson = new Gson();

    private static Type userType = new TypeToken<List<UserModel>>() {}.getType();


    public String getArticleDetails(String publicationId, String rawToken) throws IOException {

        String url = ServicePaths.PUBLICATIONS_RESOURCE_ENDPOINT + SLASH + publicationId;

        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }
    
    public String getArticleDetailsByHash(String hash, String rawToken) throws IOException {

        String url = ServicePaths.PUBLICATIONS_RESOURCE_ENDPOINT + SLASH + HASH_PATH + SLASH + hash;

        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }

    public String saveArticle(String title, String keywords, String commentsAllowed,
                              String peerReviewType, String abstractContent, String introductionContent,
                              String methodContent, String resultsContent, String discussionContent,
                              String conclusionContent, String referencesContent, String summary,
                              String email, String coAuthors, String rawToken) throws IOException {

        ArticleModel articleModel = new ArticleModel();
        articleModel.setTitle(Util.encode(title));
        articleModel.setKeywords(Util.encode(keywords));
        articleModel.setCommentsAllowed(Util.encode(commentsAllowed));
        articleModel.setAuthor(new UserModel(email));
        //articleModel.setCoAuthors(_gson.fromJson(coAuthors, userType));
        articleModel.setPeerReviewType(Util.encode(peerReviewType));
        articleModel.setAbstractContent(Util.encode(abstractContent));
        articleModel.setIntroductionContent(Util.encode(introductionContent));
        articleModel.setMethodsContent(Util.encode(methodContent));
        articleModel.setResultsContent(Util.encode(resultsContent));
        articleModel.setDiscussionContent( Util.encode(discussionContent));
        articleModel.setConclusionContent(Util.encode(conclusionContent));
        articleModel.setReferencesContent(Util.encode(referencesContent));
        articleModel.setSummary(Util.encode(summary));
        articleModel.setName(title);

        String articleDetailsJson = _gson.toJson(articleModel);

        String url = ServicePaths.PUBLICATIONS_RESOURCE_ENDPOINT;
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.POST);
        request.addHeader(CONTENT_TYPE_HEADER);
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));
        request.setPayload(articleDetailsJson.getBytes("utf8"));


        HTTPResponse httpResponse = urlFetchService.fetch(request);

        return new String(httpResponse.getContent(), "UTF-8");
    }
}
