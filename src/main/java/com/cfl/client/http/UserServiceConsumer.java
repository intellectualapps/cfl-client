package com.cfl.client.http;

import com.cfl.client.http.model.CountryModel;
import com.cfl.client.http.model.StateModel;
import com.cfl.client.http.model.TitleModel;
import com.cfl.client.http.model.UserModel;
import com.cfl.client.http.util.ServiceParams;
import com.cfl.client.http.util.ServicePaths;
import com.cfl.client.http.util.Util;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.URL;

/**
 * Created by makarfi on 12/20/17.
 */
public class UserServiceConsumer {

    private static final HTTPHeader CONTENT_TYPE_HEADER =
            new HTTPHeader("Content-Type", "application/json; charset=UTF-8");

    private static final String PARAMETER_START = "?";
    private static final String SLASH = "/";

    private static URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();
    private static Gson _gson = new Gson();


    public String getUserDetails(String email, String rawToken) throws IOException {

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + SLASH + email;

        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }

    public String updateUserDetails(String email, String firstName, String lastName,
                                    String phoneNumber, String location, String photoUrl,
                                    String titleId, String stateId, String countryId,
                                    String city, String institution, String rawToken) throws IOException {

        String url = ServicePaths.USERS_RESOURCE_ENDPOINT + SLASH + email;


        UserModel userDetails = new UserModel();
        userDetails.setEmail(Util.encode(email));
        userDetails.setFirstName(Util.encode(firstName));
        userDetails.setLastName(Util.encode(lastName));
        userDetails.setPhoneNumber(Util.encode(phoneNumber));
        userDetails.setLocation(Util.encode(location));
        userDetails.setPhotoUrl(Util.encode(photoUrl));
        userDetails.setTitle(new TitleModel(titleId));
        userDetails.setState(new StateModel(stateId));
        userDetails.setCountry(new CountryModel(countryId));
        userDetails.setCity(Util.encode(city));
        userDetails.setInstitution(Util.encode(institution));

        String userDetailsJson = _gson.toJson(userDetails);
        
        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.PUT);
        request.addHeader(CONTENT_TYPE_HEADER);
        request.addHeader(new HTTPHeader(ServiceParams.AUTHORIZATION, rawToken));
        request.setPayload(userDetailsJson.getBytes("utf8"));


        HTTPResponse httpResponse = urlFetchService.fetch(request);

        return new String(httpResponse.getContent(), "UTF-8");
    }
}
