package com.cfl.client.http;

import com.cfl.client.http.util.ServicePaths;
import com.google.appengine.api.urlfetch.*;

import java.io.IOException;
import java.net.URL;

/**
 * Created by makarfi on 1/7/18.
 */
public class TitleServiceConsumer {

    private static final HTTPHeader CONTENT_TYPE_HEADER =
            new HTTPHeader("Content-Type", "application/x-www-form-urlencoded");

    private static URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();

    public String getAllTitles() throws IOException {

        String url = ServicePaths.TITLES_RESOURCE_ENDPOINT;

        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }
}
