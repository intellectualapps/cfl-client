package com.cfl.client.http;

import com.cfl.client.http.util.ServicePaths;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

import java.io.IOException;
import java.net.URL;

/**
 * Created by makarfi on 1/7/18.
 */
public class CountryServiceConsumer {

    private static final HTTPHeader CONTENT_TYPE_HEADER =
            new HTTPHeader("Content-Type", "application/x-www-form-urlencoded");

    private static final String SLASH = "/";

    private static URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();


    public String getAllCountries() throws IOException {

        String url = ServicePaths.COUNTRIES_RESOURCE_ENDPOINT;

        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }

    public String getStatesByCountry(String countryId) throws IOException {

        String url = ServicePaths.COUNTRIES_RESOURCE_ENDPOINT + SLASH + countryId + ServicePaths.STATES_SERVICE;

        HTTPRequest request = new HTTPRequest(new URL(url), HTTPMethod.GET);
        request.addHeader(CONTENT_TYPE_HEADER);

        HTTPResponse httpResponse = urlFetchService.fetch(request);
        return new String(httpResponse.getContent(), "UTF-8");
    }

}
