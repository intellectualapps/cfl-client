package com.cfl.client.http.util;

/**
 * Created by makarfi on 12/20/17.
 */
public class ServicePaths {

    public static final String LOCAL_SERVER = "http://localhost:8080";
    public static final String REMOTE_SERVER = "http://165.227.75.157:8080";

    public static final String BASE_URL = "/cflus/api/v1";


    // USERS ENDPOINTS
    public static final String USERS_SERVICE = "/users";
    public static final String USERS_RESOURCE_ENDPOINT = REMOTE_SERVER + BASE_URL + USERS_SERVICE;

    // TITLES ENDPOINT
    public static final String TITLES_RESOURCE_ENDPOINT = REMOTE_SERVER + BASE_URL + "/titles";

    //COUNTRIES ENDPOINTS
    public static final String COUNTRIES_RESOURCE_ENDPOINT = REMOTE_SERVER + BASE_URL + "/countries";
    public static final String STATES_SERVICE = "/states";

    // ARTICLES ENDPOINTS
    public static final String PUBLICATIONS_SERVICE = "/publications";
    public static final String PUBLICATIONS_RESOURCE_ENDPOINT = REMOTE_SERVER + BASE_URL + PUBLICATIONS_SERVICE;


    public static final String USERNAME_RESOURCE = "/username";
    public static final String VERIFY_EMAIL_PATH = "/email";
    public static final String SMS_CODE_RESOURCE = "/phone/code";
    public static final String EMAIL_LINK_RESOURCE = "/email/link";
    public static final String DEVICE_TOKEN_RESOURCE = "/device-token";
    public static final String USER_AUTHENTICATION_RESOURCE = "/authenticate";
    public static final String USER_PASSWORD_RESOURCE = "/password";
    public static final String USER_PASSWORD_TOKEN_RESOURCE = "/password-token";
    public static final String USERNAME_VERIFICATION_RESOURCE = "/verify";
    public static final String LINK_SOCIAL_RESOURCE = "/link-social";
    public static final String UNLINK_SOCIAL_RESOURCE = "/unlink-social";

}
