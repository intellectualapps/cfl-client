package com.cfl.client.http.util;

/**
 * Created by makarfi on 12/20/17.
 */
public class ServiceParams {

    // USER PARAMS
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "first-name";
    public static final String LAST_NAME = "last-name";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PHOTO_URL = "photoUrl";
    public static final String LOCATION = "location";
    public static final String TITLE = "title";
    public static final String STATE = "state";
    public static final String COUNTRY = "country";
    public static final String CITY = "city";
    public static final String INSTITUTION = "institution";

    public static final String USERNAME = "username";

    public static final String ID = "id";
    public static final String AUTH_TYPE = "auth-type";


    public static final String PASSWORD = "password";

    public static final String GENDER = "gender";
    public static final String FACEBOOK_EMAIL = "facebook-email";
    public static final String TWITTER_EMAIL = "twitter-email";

    public static final String PASSWORD_RESET_TOKEN = "password-reset-token";
    public static final String AUTHORIZATION = "Authorization";
    public static final String TEMP_KEY = "temp-key";
    public static final String PLATFORM= "platform";

    // ARTICLE PARAMS
    public static final String PUBLICATION_ID = "publication-id";

    public static final String CO_AUTHORS = "co-authors";
    public static final String KEYWORDS = "keywords";
    public static final String COMMENTS_ALLOWED = "comments-allowed";
    public static final String PEER_REVIEW_TYPE = "peer-review-type";
    public static final String ABSTRACT_CONTENT = "abstract";
    public static final String INTRODUCTION_CONTENT = "introduction";
    public static final String METHODS_CONTENT = "methods";
    public static final String RESULTS_CONTENT = "results";
    public static final String DISCUSSION_CONTENT = "discussion";
    public static final String CONCLUSION_CONTENT = "conclusion";
    public static final String REFERENCES_CONTENT = "reference";
    public static final String SUMMARY = "summary";
}
