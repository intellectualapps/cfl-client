package com.cfl.client.http.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Created by makarfi on 12/20/17.
 */
public class Util {

    public static String getData(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
            sb.append('&');
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }

        return sb.toString();
    }

    public static String encode(String value)
            throws UnsupportedEncodingException {
        if (value == null) {
            return "";
        }
        return URLEncoder.encode(value.trim(), StandardCharsets.UTF_8.name());
    }
}
