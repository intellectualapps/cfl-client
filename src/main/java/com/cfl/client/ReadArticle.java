/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ahassan
 */
public class ReadArticle extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Cookie[] cookies = request.getCookies();
    boolean showDashboard = false;

    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        String name = cookies[i].getName();
        if (name.equals("cfl-authToken")) {
          showDashboard = true;
          break;
        }
      }
    }

    if (showDashboard) {
      request.getRequestDispatcher("WEB-INF/pages/read-article.html").forward(request, response);
    } else {
      response.sendRedirect("/login");
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }

  @Override
  public String getServletInfo() {
    return "Servlet to manage the read article page 'read-article.html'";
  }

}
