/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cfl.client;

import com.cfl.client.http.PublicationServiceConsumer;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ahassan
 */
public class CreatePublication extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Cookie[] cookies = request.getCookies();
    boolean showDashboard = false;

    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        String name = cookies[i].getName();
        if (name.equals("cfl-authToken")) {
          showDashboard = true;
          break;
        }
      }
    }

    if (showDashboard) {
      request.getRequestDispatcher("WEB-INF/pages/create-publication.html").forward(request, response);
    } else {
      response.sendRedirect("/login");
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String title = request.getParameter("title");
      String keywords = request.getParameter("keywords");
      String peerReviewType = request.getParameter("peer-review-type");
      String articleAbstract = request.getParameter("abstract");
      String introduction = request.getParameter("introduction");
      String methods = request.getParameter("methods");
      String results = request.getParameter("results");
      String discussions = request.getParameter("discussion");
      String conclusion = request.getParameter("conclusion");
      String references = request.getParameter("references");
      String authToken = getAuthTokenFromCookie(request);
      String email = getEmailFromCookie(request);
      System.out.println("title: " + title + " keywords: " + keywords + " peerReviewType: " + peerReviewType 
              + " abstract: " + articleAbstract + " introduction: " + introduction  
              + " methods: " + methods + " results: " + results + " discussions: " + discussions 
              + " conclusion: " + conclusion + " references: " + references + " email: " + email + " authToken: " + authToken);
      String serverResponse = new PublicationServiceConsumer().saveArticle(stripHtmlTags(title), stripHtmlTags(keywords), "", peerReviewType, 
              stripHtmlTags(articleAbstract), stripHtmlTags(introduction), stripHtmlTags(methods), stripHtmlTags(results), stripHtmlTags(discussions), stripHtmlTags(conclusion), 
              stripHtmlTags(references), "", email, "", authToken);
      System.out.println("Server response: " + serverResponse);
  }

  @Override
  public String getServletInfo() {
    return "Servlet to manage the create publication page 'create-publication.html'";
  }
  
  private String getAuthTokenFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                String name = cookies[i].getName();
                if (name.equals("cfl-authToken")) {
                    return cookies[i].getValue();
                }
            }
        }
        return "";
    }
  
  private String getEmailFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                String name = cookies[i].getName();
                if (name.equals("cfl-email")) {
                    return cookies[i].getValue();
                }
            }
        }
        return "";
    }
  
  private String stripHtmlTags(String text) {
      if (text != null && !text.isEmpty()) {
          text = text.replace("<p>", "");
          text = text.replace("</p>", "");
          return text;
      }
      
      return "";
  }

}
